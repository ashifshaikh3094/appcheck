package com.aceventura.appcheck;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.aceventura.appcheck.AppDatabase.*;

public class MainActivity extends AppCompatActivity {
    static final int RESULT_ENABLE = 1;
    static DevicePolicyManager deviceManger;
    static ActivityManager activityManager;
    public static ComponentName compName;
    final static int RQS_1 = 1;
    public static AppDatabase ad;

    static MainActivity mActivity;

    public static MainActivity getInstance() {

        if (mActivity == null) {

        }
        return mActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceManger = (DevicePolicyManager) getSystemService(
                Context.DEVICE_POLICY_SERVICE);
        activityManager = (ActivityManager) getSystemService(
                Context.ACTIVITY_SERVICE);


        compName = new ComponentName(this, MyAdmin.class);
        setContentView(R.layout.activity_main);
        mActivity = MainActivity.this;
        AlarmReceiver dd = new AlarmReceiver(mActivity);

        getLock();

        findViewById(R.id.btn_alaram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openTimePickerDialog(false);

            }
        });

        findViewById(R.id.btn_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                startActivityForResult(intent, 0);
                stopService(new Intent(getBaseContext(), MyService.class));
            }
        });


        findViewById(R.id.btn_lock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.btn_lock) {
                    getLock();
                }

            }
        });
        findViewById(R.id.btn_run).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getCallBackUp();

                String vali = "";
                // String frequencyTime = AppCheck.get_installed_app_frequency_post(MainActivity.this, "100065");

                AppCheck.getApplicationNameCode(MainActivity.this);   // todo master list   1
                AppCheck.getPermissions(MainActivity.this);// get permition
                AppCheck.setUserId(MainActivity.this, "100099");//todo user id  passing
                //work
                AppCheck.sendInstalledAppsToServer("100099", MainActivity.this);//todo app list    2
                AppCheck.sendUserDataToServer("100099", MainActivity.this);// todo app list details in appliction 3 polling time date and time spend on app listing


                ad = getAppDatabase(MainActivity.this);
                //  startService(new Intent(getBaseContext(), MyService.class));

                // AppDao dao = ad.appDao();
                Toast.makeText(getApplicationContext(), "Trying to call Rdb", Toast.LENGTH_SHORT).show();
               /* List<AppData> dd = dao.getAllApps();
                for (int i = 0; i < dd.size(); i++) {
                    AppData model = dd.get(i);

                }*/
            }


        });
    }

    public static void getLock() {
        boolean active = deviceManger.isAdminActive(compName);
        if (active) {
            deviceManger.lockNow();
        }

        Intent intent = new Intent(DevicePolicyManager
                .ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                compName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                "Additional text explaining why this needs to be added.");
        mActivity.startActivityForResult(intent, RESULT_ENABLE);

    }

    private void openTimePickerDialog(boolean b) {

        Calendar calendar = Calendar.getInstance();


        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Calendar calNow = Calendar.getInstance();
                Calendar calSet = (Calendar) calNow.clone();

                calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calSet.set(Calendar.MINUTE, minute);
                calSet.set(Calendar.SECOND, 0);
                calSet.set(Calendar.MILLISECOND, 0);

                if (calSet.compareTo(calNow) <= 0) {
                    // Today Set time passed, count to tomorrow
                    calSet.add(Calendar.DATE, 1);
                }

                setAlarm(calSet);

            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this,
                onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), false);
        timePickerDialog.setTitle("Set Alarm Time");
        Log.e("", "");
        timePickerDialog.show();
    }

    private void setAlarm(Calendar calSet) {
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(),
                pendingIntent);
        startService(new Intent(getBaseContext(), MyService.class));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_ENABLE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e("DeviceAdminSample", "Admin enabled!");
                } else {
                    Log.e("DeviceAdminSample", "Admin enable FAILED!");
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}